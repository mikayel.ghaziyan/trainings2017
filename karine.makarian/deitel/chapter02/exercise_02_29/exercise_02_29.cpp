#include <iostream>

int 
main()
{
    std::cout << "integer\tsquare\tcube\n"
              << "0\t"<< (0 * 0) << "\t" << (0 * 0 * 0) << std::endl
              << "1\t"<< (1 * 1) << "\t" << (1 * 1 * 1) << std::endl
              << "2\t"<< (2 * 2) << "\t" << (2 * 2 * 2) << std::endl
              << "3\t"<< (3 * 3) << "\t" << (3 * 3 * 3) << std::endl
              << "4\t"<< (4 * 4) << "\t" << (4 * 4 * 4) << std::endl
              << "5\t"<< (5 * 5) << "\t" << (5 * 5 * 5) << std::endl
              << "6\t"<< (6 * 6) << "\t" << (6 * 6 * 6) << std::endl
              << "7\t"<< (7 * 7) << "\t" << (7 * 7 * 7) << std::endl
              << "8\t"<< (8 * 8) << "\t" << (8 * 8 * 8) << std::endl
              << "9\t"<< (9 * 9) << "\t" << (9 * 9 * 9) << std::endl
              << "10\t"<< (10 * 10) << "\t" << (10 * 10 * 10) << std::endl;

    return 0;
}

