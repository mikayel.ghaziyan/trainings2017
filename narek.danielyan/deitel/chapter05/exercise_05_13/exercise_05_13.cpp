#include<iostream>

int
main()
{
    for (int i = 1; i <= 5; ++i) {
        int number;
        std::cout << "Input the number: ";
        std::cin >> number;
        if (number < 1 || number > 30) {
            std::cout << "Error 1:Inputing number should be between 1 and 30" << std::cout << std::endl;
            return 1;
        }
        for (int j = 1; j <= number; ++j) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0;
}
