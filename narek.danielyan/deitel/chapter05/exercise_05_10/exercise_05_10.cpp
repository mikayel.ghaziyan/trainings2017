#include<iostream>
#include<iomanip>
int
main()
{   
    int factorial = 1;
    std::cout << "Number" << std::setw(13) << "Factorial" << std::endl;
    for (int i = 1; i <= 5; ++i) {
        factorial *= i;
        std::cout << std::setw(6) << i << std::setw(13) << factorial << std::endl; 
    }
    return 0;
}
