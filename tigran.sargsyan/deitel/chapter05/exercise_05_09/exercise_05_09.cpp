#include <iostream>

int
main()
{
    int multiple = 1;
    for (int i = 1; i <= 15; i += 2) {
        std::cout << i << std::endl;
        multiple *= i;
    }

    std::cout << "Multiple is: " << multiple << std::endl;
    return 0;
}

