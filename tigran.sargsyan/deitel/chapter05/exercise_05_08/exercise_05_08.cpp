#include <iostream>
#include <limits.h>

int
main()
{
    int counts;
    std::cout << "Enter number of counts: ";
    std::cin >> counts;

    if (counts <= 0) {
        std::cerr << "Error 1. Number of counts should be positive." << std::endl;
        return 1;
    }

    int minimum = INT_MAX;
    for (int i = 1; i <= counts; ++i) {
        int number;
        std::cout << "Enter number: ";
        std::cin >> number;

        if (number < minimum) {
            minimum = number;
        }
    }

    std::cout << "Minimum: " << minimum << std::endl;
    return 0;
}

