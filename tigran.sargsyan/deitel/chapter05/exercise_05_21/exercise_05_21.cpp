#include <iostream>

int
main()
{
    int index = 1;
    double sum = 0, salary;
    while (true) {
        int payCode;
        std::cout << "Enter paycode: ";
        std::cin >> payCode;
        if (-1 == payCode) {
            break;
        }
        switch(payCode) {
        case 1:
            std::cout << "Enter salary: ";
            std::cin >> salary;
            break;
        case 2:
            std::cout << "Enter hourly wage: ";
            double hourlyWage;
            std::cin >> hourlyWage;
            std::cout << "Enter hours worked: ";
            int hours;
            std::cin >> hours;
            if (hours <= 40) {
                salary = hours * hourlyWage;
            } else {
                salary = (1.5 * hours - 20) * hourlyWage;
            }
            break;
        case 3:
            std::cout << "Enter weekly sales sum: ";
            double weeklySales;
            std::cin >> weeklySales;
            salary = 250 + 0.057 * weeklySales;
            break;
        case 4:
            std::cout << "Enter cost per item: ";
            double cost;
            std::cin >> cost;
            std::cout << "Enter amount of items sold: ";
            int amount;
            std::cin >> amount;
            salary = cost * amount;
            break;
        default:
            std::cerr << "Error 1. Wrong paycode." << std::endl;
            std::cout << "Total: " << sum << std::endl;
            return 1;
        }
        std::cout << "Employee " << index << ": " << salary << std::endl;
        sum += salary;
        ++index;
    }
    std::cout << "Total: " << sum << std::endl;
    return 0;
}

