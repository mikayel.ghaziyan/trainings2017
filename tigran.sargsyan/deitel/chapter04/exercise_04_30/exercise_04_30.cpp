#include <iostream>

int
main()
{
    double radius;
    std::cout << "Enter radius: ";
    std::cin >> radius;

    if (radius < 0) {
        std::cerr << "Error 1. Radius should be non-negative." << std::endl;
        return 1;
    }

    std::cout << "Diameter: " << (2 * radius) << std::endl;
    std::cout << "Circumference: " << (2 * 3.14159 * radius) << std::endl;
    std::cout << "Area: " << (3.14159 * radius * radius) << std::endl;
    return 0;
}

