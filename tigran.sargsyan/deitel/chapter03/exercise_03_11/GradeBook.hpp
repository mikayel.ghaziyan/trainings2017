#include <iostream>
#include <string>

class GradeBook
{
    public:
        GradeBook(std::string nameOfCourse, std::string nameOfInstructor);
        void setCourseName(std::string nameOfCourse);
        std::string getCourseName();
        void setInstructorName(std::string nameOfInstructor);
        std::string getInstructorName();
        void displayMessage();

    private:
        std::string courseName_;
        std::string instructorName_;
};

