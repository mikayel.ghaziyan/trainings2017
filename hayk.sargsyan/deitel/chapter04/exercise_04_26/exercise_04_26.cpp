#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter five digit number: ";
    std::cin >> number;

    int character1 = number % 10;
    int character2 = number / 10 % 10;
    int character4 = number / 1000 % 10; 
    int character5 = number / 10000 % 10;
    
    if (character1 == character5) {
        if (character2 == character4) {
            std::cout << number << " is a palindrome number" << std::endl;
            
            return 0;
        } 
    }

    std::cout << number << " is a non-palindrome number" << std::endl;
    
    return 0;
}
