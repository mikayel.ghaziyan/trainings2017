#include <iostream>
#include <assert.h>

void
drawSquare(const int side) 
{
    assert(side > 0);
    for (int counterY = 1; counterY <= side; ++counterY) {
        for (int counterX = 1; counterX <= side; ++counterX) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }
}

int
main()
{
    int side;

    std::cout << "Please enter the side: ";
    std::cin >> side;

    if (side < 1) {
        std::cerr << "Error 1: Side can't be zero or negative" << std::endl;
        return 1;
    }

    drawSquare(side);

    return 0;
}
