#include <iostream>

int
main()
{
    for (int number = 1; number <= 256; number++) {

        int index = 1;
        int counter = number;
        int binary = 0;
        int binaryCharacter;

        while (counter != 0) {
            binaryCharacter = counter % 2;
            counter /= 2;
            binary += binaryCharacter * index;
            index *= 10;
        }

        std::cout << "binary: " << binary 
                  << " - octal: " << std::oct << number 
                  << " - hexadecimal: " << std::hex << number
                  << " - decimal: " << std::dec << number 
                  << std::endl;
    }

    return 0;
}
