#include <iostream>
#include <cmath>

int
main()
{
    int size;

    std::cout << "Please enter the size (1-19): ";
    std::cin >> size;

    if (size < 1 || size > 19) {
        std::cerr << "Error 1: Wrong size entered." << std::endl;
        return 1;
    }

    size /= 2;

    for (int counterY = -size; counterY <= size; ++counterY) {
        for (int counterX = -size; counterX <= size; ++counterX) {
            std::cout << ((std::abs(counterX) + std::abs(counterY) <= size) ? "*" : " ");
        }

        std::cout << std::endl;
    }
    
    return 0;
}
