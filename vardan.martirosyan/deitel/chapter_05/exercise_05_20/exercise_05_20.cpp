#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << "First side" << std::setw(17) << "Second side" << std::setw(17) << "Hypotenuse" << std::endl; 

    for (int firstSide = 3; firstSide < 498; ++firstSide) {
        int squareFirstSide = (firstSide * firstSide); 
        for (int secondSide = firstSide + 1; secondSide < 499; ++secondSide) {
            int squareSecondSide = (secondSide * secondSide); 
            for (int hypotenuse = secondSide + 1; hypotenuse <= 500 && hypotenuse < firstSide + secondSide - 1; ++hypotenuse){
                int squareHypotenuse =  (hypotenuse * hypotenuse);             
                if (squareHypotenuse == (squareFirstSide + squareSecondSide)) {
                    std::cout << firstSide << std::setw(17) << secondSide << std::setw(17) << hypotenuse << std::endl;
                    break;
                }
            }
        }
    }
    return 0;
}

