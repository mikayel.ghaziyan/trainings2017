#include "MiniComputer.hpp"
#include <string>
#include <iostream>
#include <unistd.h>

MiniComputer::MiniComputer(std::string name)
{
    setComputerName(name);
}

std::string
MiniComputer::getComputerName()
{
    return computerName_;
}

void
MiniComputer::setComputerName(std::string name)
{
    if (name.length() > 25) {
        computerName_ = name.substr(0, 25);
        return;
    }

    computerName_ = name;
}

int
MiniComputer::run()
{
    std::cout << getComputerName() << std::endl; /// getter
start:
    showMainMenu();
    int command = getMainCommand();
    if (0 == command) { /// exit command
        return 0;
    }
    int returnValue = executeCommand(command);
    if (0 == returnValue) { /// everything is ok
        goto start;
    }
    return returnValue;
}

void
MiniComputer::showMainMenu()
{
    if(::isatty(STDIN_FILENO)) {
        std::cout << "Command Set\n"
                  << "0 – exit\n"
                  << "1 – load\n"
                  << "2 – store\n"
                  << "3 – print\n"
                  << "4 – add\n\n"
                  << "> Command: ";
    }
}

int
MiniComputer::getMainCommand()
{
    int command;
    std::cin >> command;
    return command;
}

int
MiniComputer::executeCommand(int command)
{
    if (1 == command) { /// load command
        return executeLoadCommand();
    }
    if (2 == command) { /// store command
        return executeStoreCommand();
    }
    if (3 == command) { /// print command
        return executePrintCommand();
    }
    if (4 == command) { /// add command
        return executeAddCommand();
    }
    /// error case
    std::cerr << "Error 1: Command not found!" << std::endl;
    return 1;
}

int
MiniComputer::loadRegister(std::string registerName)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Load into " << registerName << "\n"
                  << "Input the value to load into register " << registerName << ".\n"
                  << registerName << ": ";
    }
    int number;
    std::cin >> number;
    std::cout << registerName << " = " << number << "\n" << std::endl;
    return number;
}

bool
MiniComputer::isRegisterError(int registerNumber)
{
    if (registerNumber > 3) {
        std::cerr << "Error 2: Register not found" << std::endl;
        return true;
    }
    if (registerNumber < 0) {
        std::cerr << "Error 2: Register not found" << std::endl;
        return true;
    }
    return false;
}

int
MiniComputer::executeLoadCommand()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Load (into one of the following registers)\n"
                  << "0 - a\n"
                  << "1 - b\n"
                  << "2 - c\n"
                  << "3 - d\n\n"
                  << "> Register: ";
    }
    int registerNumber;
    std::cin >> registerNumber;
    if (isRegisterError(registerNumber)) {
        return 2;
    }
    std::string registerName = getRegisterName(registerNumber);
    int registerValue = loadRegister(registerName);
    setRegisterValue(registerNumber, registerValue);
    return 0;
}

int
MiniComputer::executeStoreCommand()
{
    std::cerr << "Error 127: The store command is not implemented yet" << std::endl;
    return 127;
}

int
MiniComputer::getRegisterValue(int registerNumber)
{
    if (0 == registerNumber) {
        return registerA_;
    }
    if (1 == registerNumber) {
        return registerB_;
    }
    if (2 == registerNumber) {
        return registerC_;
    }
    if (3 == registerNumber) {
        return registerD_;
    }
    std::cerr << "Assert 1: Invalid register number" << std::endl;
    return 0;
}

std::string
MiniComputer::getRegisterName(int registerNumber)
{
    if (0 == registerNumber) {
        return "a";
    }
    if (1 == registerNumber) {
        return "b";
    }
    if (2 == registerNumber) {
        return "c";
    }
    if (3 == registerNumber) {
        return "d";
    }
    std::cerr << "Assert 1: Invalid register number" << std::endl;
    return "";
}

void
MiniComputer::setRegisterValue(int registerNumber, int registerValue)
{
    if (0 == registerNumber) {
        registerA_ = registerValue;
    } else if (1 == registerNumber) {
        registerB_ = registerValue;
    } else if (2 == registerNumber) {
        registerC_ = registerValue;
    } else if (3 == registerNumber) {
        registerD_ = registerValue;
    } else {
        std::cerr << "Assert 1: Invalid register number" << std::endl;
    }
}

int
MiniComputer::executeAddCommand()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Add (Register 1 + Register 2 = Register 3)\n"
                  << "0 - a\n"
                  << "1 - b\n"
                  << "2 - c\n"
                  << "3 - d\n\n"
                  << "> Register1: ";
    }

    int registerNumber1;
    std::cin >> registerNumber1;
    if (isRegisterError(registerNumber1)) {
        return 2;
    }
    int inputRegister1 = getRegisterValue(registerNumber1);
    std::string registerLetter1 = getRegisterName(registerNumber1);

    int registerNumber2;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "> Register2: ";
    }
    std::cin >> registerNumber2;
    if (isRegisterError(registerNumber2)) {
        return 2;
    }
    int inputRegister2 = getRegisterValue(registerNumber2);
    std::string registerLetter2 = getRegisterName(registerNumber2);

    int registerNumber3;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "> Register3: ";
    }
    std::cin >> registerNumber3;
    if (isRegisterError(registerNumber3)) {
        return 2;
    }
    int inputRegister3 = inputRegister1 + inputRegister2;
    setRegisterValue(registerNumber3, inputRegister3);
    std::string registerLetter3 = getRegisterName(registerNumber3);
    std::cout << registerLetter3 << " = " << registerLetter1 << " + " << registerLetter2 << " = " << inputRegister1 << " + " << inputRegister2 << " = " << inputRegister3 << "\n" << std::endl;
    return 0;
}

int
MiniComputer::executePrintCommand()
{
    int commandRegister;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Print (Register or String)\n"
                  << "0 – Register\n"
                  << "N – String Length\n\n"
                  << "> Print: ";
    }
    std::cin >> commandRegister;
    if (0 == commandRegister) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Registers\n"
                      << "0 - a\n"
                      << "1 - b\n"
                      << "2 - c\n"
                      << "3 - d\n\n"
                      << "> Register: ";
        }
        int registerNumber;
        std::cin >> registerNumber;
        if (isRegisterError(registerNumber)) {
            return 2;
        }
        std::cout << getRegisterValue(registerNumber);
        if (::isatty(STDIN_FILENO)) {
            std::cout << "\n\n";
        }
        return 0;
    }
    if (commandRegister > 0) {
        std::string line;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "> String: ";
            std::cin.ignore(commandRegister, '\n');
        }
        std::getline(std::cin, line);
        if (static_cast<int>(line.length()) > commandRegister) {
            line = line.substr(0, commandRegister);
        }
start:
        if (static_cast<int>(line.length()) < commandRegister) {
            line += " ";
            goto start;
        }
        std::cout << line;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "\n\n";
        }
        return 0;
    }
    std::cerr << "Error 2: Register not found!" << std::endl;
    return 2;
}
