#include <iostream>
int 
main()
{
    int count; // control variable also used after loop terminates
    for (count = 1; count < 5; ++count) { // loop 10 times
        std::cout << count << " ";
    } // end for
    std::cout << "\nBroke out of loop at count = " << count << std::endl;

    return 0; // indicate successful termination
} // end main
