#include <iostream>
#include <cmath>
#include <unistd.h>

double distanceBetweenTwoPoints(const double x1, const double y1, const double x2, const double y2);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert first  point x cordinat. ";
    }
    double x1;
    std::cin >> x1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert first  point y cordinat. ";
    }
    double y1;
    std::cin >> y1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert second point x cordinat. ";
    }
    double x2;
    std::cin >> x2;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert second point y cordinat. ";
    }
    double y2;
    std::cin >> y2;
    std::cout << "The distance between to points is. " 
              << distanceBetweenTwoPoints(x1, y1, x2, y2) << std::endl;

    return 0;
}

double 
distanceBetweenTwoPoints(const double x1, const double y1, const double x2, const double y2)
{
    double xCordinatsSumm;
    double x1Absolute = std::abs(x1);
    double x2Absolute = std::abs(x2);
    if (((x1 <= 0) && (x2 <= 0)) || ((x1 >= 0) && (x2 >= 0))) {
        xCordinatsSumm = std::abs(x2Absolute - x1Absolute);
    } else if (((x1 < 0) && (x2 > 0)) || ((x1 > 0) && (x2 < 0))) {
        xCordinatsSumm = x2Absolute + x1Absolute;
    }
    double yCordinatsSumm;
    double y1Absolute = std::abs(y1);
    double y2Absolute = std::abs(y2);
    if (((y1 <= 0) && (y2 <= 0)) || ((y1 >= 0) && (y2 >= 0))) {
        yCordinatsSumm = std::abs(y2Absolute - y1Absolute);
    } else if (((y1 < 0) && (y2 > 0)) || ((y1 > 0) && (y2 < 0))) {
        yCordinatsSumm = y2Absolute + y1Absolute;
    }
    double distance = std::sqrt(xCordinatsSumm * xCordinatsSumm + yCordinatsSumm * yCordinatsSumm);

    return distance;
}
