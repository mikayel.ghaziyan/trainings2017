Question!!!!.
Find the error in each of the following program segments and explain how to correct it:
------------------------------------------------------------------------------------------------------
a. float cube( float ); // function prototype
   double cube( float number ) // function definition
   {
       return number * number * number;
   }
------------------------------------------------------------------------------------------------------
b. register auto int x = 7;
------------------------------------------------------------------------------------------------------
c. int randomNumber = srand();
------------------------------------------------------------------------------------------------------
d. float y = 123.45678;
   int x;
   x = y;
   cout << static_cast < float > ( x ) << endl;
------------------------------------------------------------------------------------------------------
e. double square( double number )
   {
       double number;
       return number * number;
   }
------------------------------------------------------------------------------------------------------
f. int sum( int n )
   {    
       if ( n == 0 )
           return 0;
       else
           return n + sum( n );
   }


Answer!!!!.
------------------------------------------------------------------------------------------------------
a. float cube( float ); ///// This is function prototype
   float cube( float number ) ///// function definition must have the same argument type and same 
   returning type as function prototype

   {
       return number * number * number;
   }
------------------------------------------------------------------------------------------------------
b. register int x = 7;////variable type can't have two memory specificators, or register, or auto.
I think register much better(because every local variable automaticaly have specificator auto....)
------------------------------------------------------------------------------------------------------
c. int randomNumber = rand();////wrong srand() can not be assigned to variable because srand() 
function with argument(time(0) from ctime lib or any number), generates a new number randomness for 
rand() function, after each runing of program.I think rand() much better for this situation.
------------------------------------------------------------------------------------------------------
d. float y = 123.45678;
   float x;///I change x to a float type.
   x = y;///logical error. Assignment float value to integer variable may bring to a data loss.
   ////// I think x must be a float type or a double type variable.
   cout << x << endl;  /// And x must be displayed without operation of 
   type convertion.
------------------------------------------------------------------------------------------------------
e. double square(double number)
   {
       double number;///wrong!!!. square function takes from main() number as argument and returns 
  ///number square. Again variable declaration in function with the same name as function argument 
  ///give a wrong result. This line must be deleted.

       return number * number;
   }
------------------------------------------------------------------------------------------------------
f. int sum(int n)
   {
       if (n == 0)
           return 0;
       else
           return n + sum(n - 1); //////I think for this recursion argument of sum in this line 
           ////must be n - 1.
------------------------------------------------------------------------------------------------------
                                                                         33,1        
