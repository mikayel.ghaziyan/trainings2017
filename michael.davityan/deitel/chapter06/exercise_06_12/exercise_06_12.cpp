#include <iostream>
#include <iomanip>
#include <cmath>

float calculateCharges(const float parkingTime);

int
main() 
{
    std::cout << "Car\t" << "Hour\t" << "Charge\n"; 

    float totalTime = 0;
    float totalPayment = 0;
    for (int car = 1; car <= 3; ++car) {
        float parkingTime;
        std::cin >> parkingTime;
        if (parkingTime < 0 || parkingTime > 24) {
            std::cout << "Error 1: wrong park time.\n";
            return 1;
        }

        std::cout << car << "\t" << std::setprecision(1) << std::fixed << parkingTime << "\t"; 
        std::cout << std::setprecision(2) << std::fixed << calculateCharges(parkingTime) << "\n";
        totalTime += parkingTime;
        totalPayment += calculateCharges(parkingTime);
    }
    std::cout << "TOTAL\t" << totalTime << "\t" << totalPayment << std::endl;

    return 0;
}

float
calculateCharges(const float parkingTime) 
{
    float parkingPayment = 2;

    if (parkingTime > 3) {
        parkingPayment = 2 + std::ceil(parkingTime - 3) * 0.5;
    }
    if (parkingPayment > 10) {
        parkingPayment = 10;
    }

    return parkingPayment;
}
