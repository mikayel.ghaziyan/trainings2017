#include <iostream>

int
main()
{
    int palindrome;

    std::cin >> palindrome; /// variable for user's inserted five-digit number(to check is it palindrom or not?).

    if (palindrome > 99999) {
        std::cout << "Error 1: The input is not five digit number." << std::endl;
        return 1;
    }
    if (palindrome < 10000) {
        std::cout << "Error 1: The input is not five digit number." << std::endl;
        return 1;
    }

    int palindromeLeftSide = palindrome / 1000; /// variable for palindrome left mirror section.
    int palindromeRightSide = (palindrome % 10 * 10) + (palindrome / 10 % 10); ///variable for palindrome right mirror section.

    if (palindromeLeftSide == palindromeRightSide) {
        std::cout << "Your number palindrome" << std::endl;
    } else {
        std::cout << "Your number not palindrome" << std::endl;
    }
    
    return 0;
}
